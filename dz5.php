
<?php
    require("dz5_auth.php");
    $error = "";
    session_start();

    if ( isset( $_GET['logout'] ) )  logout();

    if ( isset( $_SESSION['username'] ) ) header( "location: /".$_COOKIE['path'].".php" );

    if ( isset($_POST['username']) && isset($_POST['password']) ) {
        if (  auth( $_POST['username'], $_POST['password'] ) ) {
            $_SESSION['username'] = "admin";
            $_SESSION['id'] = 1;
            if (isset($_COOKIE['path'])) header("location: /" . $_COOKIE['path'].".php");
            else header("location: /dz5_A.php");
        }else{ $error = "Wrong username or password!"; }
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="style/s1.css" />
</head>
<body>

    <div id="login">
        <form method="post">
            <?= $error;?>
            Login:    <input type="text" name="username"><br>
            Password: <input type="password" name="password">
            <br><input type="submit" value="Log in">
        </form>
    </div>

</body>
</html>