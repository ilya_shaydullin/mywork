<?php
//require("dz5_auth.php");

    session_start();
    if ( !isset( $_SESSION['username'] ) ) header( "location: /dz5.php" );
    setcookie('path','dz5_A', time() + 3600 * 24 * 7 );
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>A</title>
    <link rel="stylesheet" type="text/css" href="style/<?php if ( isset( $_COOKIE['style'] ) ) echo $_COOKIE['style'];
    else echo "s1"
    ?>.css" />
</head>
<body>

    <div id="A">
        HELLO WORLD, <?= $_SESSION["username"] ?>! IT IS A PAGE!

        <br><a href="dz5_B.php">Link to B</a>
        <br><a href="dz5_settings.php">Settings</a>
        <br><a href="dz5.php?logout=yes">Logout</a>
    </div>

</body>
</html>