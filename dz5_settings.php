<?php
    session_start();

    if ( !isset( $_SESSION['username'] ) ) header( "location: /dz5.php" );

    if ( isset( $_POST['style'] ) ) { setcookie('style', $_POST['style'], time() + 3600 * 24 * 7); header("location: /dz5_settings.php"); }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Settings</title>
    <link rel="stylesheet" type="text/css" href="style/<?php if ( isset( $_COOKIE['style'] ) ) echo $_COOKIE['style'];
    else echo "s1"
    ?>.css" />
</head>
<body>

<div id="set">
    HELLO WORLD, <?= $_SESSION["username"] ?>! IT IS SETTINGS PAGE!

    <form method="post">
        <select name="style">
            <option <?php if ( isset( $_COOKIE['style'] ) && ( $_COOKIE['style'] == "s1" ) ) echo "selected" ?> value="s1">Style1</option>
            <option <?php if ( isset( $_COOKIE['style'] ) && ( $_COOKIE['style'] == "s2" ) ) echo "selected" ?> value="s2">Style2</option>
            <option <?php if ( isset( $_COOKIE['style'] ) && ( $_COOKIE['style'] == "s3" ) ) echo "selected" ?> value="s3">Style3</option>
        </select>
        <input type="submit" value="Save!">
    </form>

    <br><a href="dz5_A.php">Link to A</a>
    <br><a href="dz5_B.php">Link to B</a>
    <br><a href="dz5.php?logout=yes">Logout</a>
</div>

</body>
</html>
