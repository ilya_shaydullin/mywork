<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 25.02.2016
 * Time: 18:45
 */

//phpinfo(); exit;
require "dz7_func.php";

$error = "";
//$cnt = countImages('img/') + 1;
if (isset( $_FILES['image'] ) && $_POST['name'] != "" && $_POST['description'] != "" ){

    if (($_FILES['image']['error'] == 2) || ($_FILES['image']['size'] > 2000000)) {
        echo "File is more 2Mb!";
    } elseif ($_FILES['image']['type'] != "image/jpeg") echo "File is not jpeg!";

    elseif ($_FILES['image']['error'] == 0) {
        $lnk = md5(uniqid(rand(), true));
        $uploadFile = "img/" . $lnk;

        if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadFile)) {
            $im = imagecreatefromjpeg($uploadFile);

            $ox = imagesx($im);
            $oy = imagesy($im);
            $nx = 200;
            $ny = floor($oy * (200 / $ox));
            $nm = imagecreatetruecolor($nx, $ny);

            imagecopyresized($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);
            imagejpeg($nm, "img/thumbnail/" . $lnk);

            $mysqli->autocommit(false);
                if ( !( $mysqli->query( 'INSERT INTO `photo` (`name`, `description`, `link`)
VALUES ("'.$_POST['name'].'", "'.$_POST['description'].'", "'.$lnk.'" )') ) ||
                    !( $res = $mysqli->query( ' SELECT `id` FROM `photo` WHERE `name` = "'.$_POST['name'].'"')->fetch_assoc() ) || //( ( $row = $res->fetch_assoc() ) != "" ) ||
                    !$mysqli->query( 'INSERT INTO `popularity` (`id_photo`) VALUES ('.$res['id'].')' )
                ){
                    unlink($uploadFile);
                    unlink("img/thumbnail/" . $lnk);
                    echo "Не удалось сохранить файл: (" . $mysqli->errno . ") " . $mysqli->error."<br>".$res['id'];
                    $mysqli->rollback();
                    //print_r($res);
                }else {
                    $mysqli->commit();
                }
            $mysqli->autocommit(true);

        } else echo "File is not upload!!";

    } else echo "Error #" . $_FILES['image']['error'];

}//echo "Field(s) is(are) empty!";

if (isset($_GET['createtbls'])) {
    if (!$mysqli->query("DROP TABLE IF EXISTS `popularity`") ||
        !$mysqli->query("DROP TABLE IF EXISTS `photo`") ||
        !$mysqli->query("CREATE TABLE `photo`(
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(32) NOT NULL,
  `link` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;") ||
        !$mysqli->query("CREATE TABLE `popularity`(
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_photo` int(11) UNSIGNED NOT NULL,
  `count` int(11),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_photo`) REFERENCES `photo` (`id`) ON DELETE CASCADE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        echo "Не удалось создать таблицу: (" . $mysqli->errno . ") " . $mysqli->error;
    }
}

if ( isset( $_GET['insert'] ) ){
    $mysqli->autocommit(false);
    for ($i = 1; $i <= (countImages('img/')); $i++) {
        if ( !( $mysqli->query( 'INSERT INTO `photo` (`name`, `description`, `link`) VALUES ("Photo#'.$i.'", "Description#'.$i.'", "'.$i.'.jpg" )') ) ||
            !( $res = $mysqli->query( ' SELECT `id` FROM `photo` WHERE `name` = "Photo#'.$i.'"')->fetch_assoc() ) || //( ( $row = $res->fetch_assoc() ) != "" ) ||
            !$mysqli->query( 'INSERT INTO `popularity` (`id_photo`) VALUES ('.$res['id'].')' )
        ){
            $mysqli->rollback();
            echo "Не удалось создать запись: (" . $mysqli->errno . ") " . $mysqli->error."<br>".$res['id'];
            //print_r($res);
        }else {
            $mysqli->commit();
        }
    }
    $mysqli->autocommit(true);
}


?>
<!DOCTYPE html>
<html>
<head>
    <title>Photo gallery</title>
</head>
<body>

<h1>Photo gallery</h1>
<?php

if ( !( $imgs = $mysqli->query(
    'SELECT `photo`.`link`, `photo`.`name`, `photo`.`id` FROM `photo`, `popularity`
WHERE `photo`.`id` = `popularity`.`id_photo` ORDER BY `popularity`.`count` DESC' ) ) )
    echo "Не удалось загрузить данные: (" . $mysqli->errno . ") " . $mysqli->error;

while ($img = $imgs->fetch_assoc()) { ?>
    <a href="dz7_photo.php?id=<?= $img['id'] ?>"> <img src='img/thumbnail/<?= $img['link'] ?>'></a>
<?php } ?>

<!--<br><br>
<a href="dz7.php?createtbls=1">Create tables (photo, popularity)</a>
<br><br>
<a href="dz7.php?insert=1">Insert into</a>-->
<br><br>
<form enctype="multipart/form-data" method="post">
    <br>Name: <br><input name="name" type="text">
    <br>Description: <br><input name="description" type="text">
    <br><br><input type="hidden" name="MAX_FILE_SIZE" value="2000000"/>
    Upload photo: <input accept="image/jpeg" name="image" type="file"/><br>
    <input type="submit" value="Send File"/>
</form>

</body>
</html>
