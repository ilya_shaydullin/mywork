<?php

//Задание 1
echo "<br><br>Задание 1<br><br>";
$i = 0;
while($i <= 100){
    if($i % 3 == 0) echo $i.", ";
    $i++;
}

//Задание 2
echo "<br><br>Задание 2<br><br>";

$i = PHP_INT_MAX;
echo "\nИсходное значение: $i <br>";

$startTime = microtime();
//do{
    if($i == 0) echo $i."- Это ноль<br>";
    elseif( ($i % 2) == 0)  echo $i." - это чётное число<br>";
    else echo $i." - это нечётное число<br>";
 //   $i++;
//}while($i != 1000000);
$endTime = microtime();
echo "Time(1000000 - division) = ".($endTime - $startTime)."<br><br>";

$startTime = microtime();
//$i = 0;
//do{
    if($i == 0) echo $i."- Это ноль<br>";
    elseif( ($i & 1) == 0) echo $i." - это чётное число<br>";
    else echo $i." - это нечётное число<br>";
    //$i++;
//}while($i != 1000000);
$endTime = microtime();
echo "Time(1000000 - bit operation) = ".($endTime - $startTime);


//Задание 3
echo "<br><br>Задание 3<br><br>";

for($i = 0; $i <= 9; print($i++.". ") );

//Задание 4
echo "<br><br>Задание 4<br><br>";

$city = array(
    "Московская область" => array("Москва", "Зеленоград", "Клин"),
    "Ленинградская область" => array("Санкт-Петербург", "Всеволожск", "Павловск", "Кронштадт"),
    "Рязанская область" => array("Касимов", "Рязань", "Сасово", "Скопин"),
);

foreach($city as $key => $value){
    echo $key.": <br>";
    echo "<pre>";
    foreach($value as $podValue){
        echo $podValue.", ";
    }
    echo "</pre><br>";
}

//Задание 5
echo "<br><br>Задание 5<br><br>";

foreach($city as $key => $value){
    echo $key.": <br>";
    echo "<pre>";
    foreach($value as $podValue){
        if ( strstr ( $podValue, 'К' ) != false ) echo $podValue.", ";
    }
    echo "</pre><br>";
}

//Задание 6
echo "<br><br>Задание 6<br><br>";

$trans = array(
    "А"=>"A","а"=>"a",
    "Б"=>"B","б"=>"b",
    "В"=>"V","в"=>"v",
    "Г"=>"G","г"=>"g",
    "Д"=>"D","д"=>"d",
    "Е"=>"E","е"=>"e",
    "Ё"=>"E","ё"=>"e",
    "Ж"=>"Zh","ж"=>"zh",
    "З"=>"Z","з"=>"z",
    "И"=>"I","и"=>"i",
    "Й"=>"I","й"=>"i",
    "К"=>"K","к"=>"k",
    "Л"=>"L","л"=>"l",
    "М"=>"M","м"=>"m",
    "Н"=>"N","н"=>"n",
    "О"=>"O","о"=>"o",
    "П"=>"P","п"=>"p",
    "Р"=>"R","р"=>"r",
    "С"=>"S","с"=>"s",
    "Т"=>"T","т"=>"t",
    "У"=>"U","у"=>"u",
    "Ф"=>"F","ф"=>"f",
    "Х"=>"Kh","х"=>"kh",
    "Ц"=>"Tc","ц"=>"tc",
    "Ч"=>"Ch","ч"=>"ch",
    "Ш"=>"Sh","ш"=>"sh",
    "Щ"=>"Shch","щ"=>"shch",
    "Ы"=>"Y","ы"=>"y",
    "Э"=>"E","э"=>"e",
    "Ю"=>"Iu","ю"=>"iu",
    "Я"=>"Ia","я"=>"ia",
    "ъ"=>"","ь"=>"",
);


function translit($str){
    return  strtr( $str, $GLOBALS["trans"]) ;
}

echo translit("Привет, мир!");


//Задание 7
echo "<br><br>Задание 7<br><br>";

function spaceToUnderline($str){
    return  str_replace( " ", "_", $str ) ;
}

echo spaceToUnderline("edfef efw efw ef ef");

//Задание 8
echo "<br><br>Задание 8<br><br>";

 echo $strIn = "Впервые в истории человечество зафиксировало гравитационные волны —<br>
 колебания пространства-времени, пришедшие на землю от катастрофы, произошедшей далеко во Вселенной.";

function convertToURL($str){

    return spaceToUnderline( translit($str) );
};

echo "<br><br>".convertToURL($strIn);

echo "<br><br><br><br><br>";












