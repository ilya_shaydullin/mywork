<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 26.02.2016
 * Time: 20:53
 */

include("Model/gallery.php");

if (isset( $_FILES['image'] ) && $_POST['name'] != "" && $_POST['description'] != "" ){
    fileUpload($_FILES['image'], $_POST['name'], $_POST['description']);
}

if (isset($_GET['createtbls'])) {
    createTables();
}

if (isset($_GET['insert'])) {
    insertItems();
}

//print_r( scandir('img/') );

$title = "Photo gallery";

$photos = getAllPhotos();

if ( isset( $_GET['view'] ) ) $content = "View/dz8_list.php";
else  $content = "View/dz8_table.php";

include("View/main.php");
