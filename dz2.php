Задание 1 <br><br>
<?php
// Задание 1
$a = 23;
$b = 0;

if( ( ($a >= 0) and ($b >= 0) ) ) echo $a - $b;
elseif( ($a < 0) and ($b < 0) ) echo $a * $b;
else echo $a + $b;

//Задание 2
echo "<br><br>Задание 2<br><br>";
$a = 10;

switch($a){
    case 0: echo "1<br>";
    case 1: echo "1<br>";
    case 2: echo "2<br>";
    case 3: echo "3<br>";
    case 4: echo "4<br>";
    case 5: echo "5<br>";
    case 6: echo "6<br>";
    case 7: echo "7<br>";
    case 8: echo "8<br>";
    case 9: echo "9<br>";
    case 10: echo "10<br>";
    case 11: echo "11<br>";
    case 12: echo "12<br>";
    case 13: echo "13<br>";
    case 14: echo "14<br>";
    case 15: echo "15<br>";
}

//Задание 3
echo "<br><br>Задание 3<br><br>";

function addit($x = 0, $y = 0){
    return $x + $y;
}
function diff($x = 0, $y = 0){
    return $x - $y;
}
function multip($x = 0, $y = 0){
    return $x * $y;
}
function mod($x = 0, $y = 0){
    return $x % $y;
}

echo addit(1, 2);
echo "<br>".diff(1, 2);
echo "<br>".multip(1, 2);
echo "<br>".mod(1, 2);

//Задание 4
echo "<br><br>Задание 4<br><br>";
function mathOperation($x = 0, $y = 0, $operation){
    switch($operation){
        case "addit": return $x + $y;
        case "diff": return $x - $y;
        case "multip": return $x * $y;
        case "mod": return $x % $y;
    }
}

echo mathOperation(1, 2, "addit")."<br>";
echo mathOperation(1, 2, "diff")."<br>";
echo mathOperation(1, 2, "multip")."<br>";
echo mathOperation(1, 2, "mod")."<br>";

//Задание 5
echo "<br><br>Задание 5<br><br>";

function power($x, $y){
    if($y == 0){
        return 1;
    }
    if($y < 0){
        return power( 1/$x, -$y);
    }
    return $x * power($x, $y-1);
}

echo power(2,2);

//Задание 6
echo "<br><br>Задание 6<br><br>";

function myRusDate($H, $i){
    $hour = "";
    $minute = "";
    if ( ( ($H % 10) == 1 ) and ( floor( $H / 10 ) != 1) ) $hour = "час";
    elseif( ( in_array( ($H % 10), [2,3,4] ) ) and ( floor( $H / 10 ) != 1) ) $hour = "часа";
    elseif( ( in_array( ($H % 10), [0, 5,6,7,8,9] ) ) or ( floor( $H / 10 ) == 1) ) $hour = "часов";

    if ( ( ($i % 10) == 1 ) and ( floor( $i / 10 ) != 1) ) $minute = "минута";
    elseif( ( in_array( ($i % 10),[2,3,4] ) ) and ( floor( $i / 10 ) != 1) ) $minute = "минуты";
    elseif( ( in_array( ($i % 10), [0,5,6,7,8,9] ) ) or ( floor( $i / 10 ) == 1) ) $minute = "минут";

    return $H." ".$hour." : ".$i." ".$minute;
}


function rus_date($hours, $minutes)
{
//    if (is_null($timestamps)) {
//        $timestamps = time();
//    }
    //$hours = date("G", $timestamps);
    //$minutes = date("i", $timestamps);

    $rus_hours = '';
    $rus_minutes = '';

    if ($hours == 1 || $hours == 21) {
        $rus_hours = 'час';
    } elseif ($hours >= 2 && $hours <= 4 || $hours >= 22 && $hours <= 24) {
        $rus_hours = 'часа';
    } elseif ($hours >= 5 && $hours <= 20) {
        $rus_hours = 'часов';
    }

    if ((int)$minutes % 10 == 1) {
        $rus_minutes = 'минута';
    } elseif ((int)$minutes % 10 >= 2 && (int)$minutes % 10 <= 4) {
        $rus_minutes = 'минуты';
    } elseif ((int)$minutes % 10 == 0 || (int)$minutes % 10 >= 5 && (int)$minutes % 10 <= 9) {
        $rus_minutes = 'минут';
    }

    return $hours . ' ' . $rus_hours . ' ' . $minutes . ' ' . $rus_minutes;
}
echo "<br>Функция Ильи:<br>";
$startTime = microtime();
for ($h = 1; $h<=24; $h++){
    echo myRusDate($h, 1)."<br>";
}
for ($m = 0; $m<=60; $m++){
    echo myRusDate(1, $m)."<br>";
}
$endTime = microtime();

echo "Время= ".($endTime - $startTime);

echo "<br><br>Функция Александра:<br>";

$startTime = microtime();
for ($h = 1; $h<=24; $h++){
    echo rus_date($h, 1)."<br>";
}
for ($m = 0; $m<=60; $m++){
    echo rus_date(1, $m)."<br>";
}
$endTime = microtime();

echo "Время= ".($endTime - $startTime);

//echo  2;




















