<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 23.02.2016
 * Time: 18:01
 */

function countImages($dir){
    $count = 0;
    $elements = scandir($dir);
    foreach( $elements as $value ){
        if ( ( $value != '..' ) && ( $value != '.' ) && !is_dir( $dir.$value ) ) $count++;
    }
    return $count;
}

//echo countImages('img/');

$error = "";
$cnt = countImages('img/') + 1;
$uploadFile = "img/".$cnt.".jpg";
if ( isset( $_FILES['image'] ) ){

    if ( ( $_FILES['image']['error'] == 2 ) || ( $_FILES['image']['size'] > 2000000 ) ) {  echo "File is more 2Mb!";  }

    elseif ( $_FILES['image']['type'] != "image/jpeg" ) echo "File is not jpeg!";

    elseif ( $_FILES['image']['error'] == 0 ) {

        if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadFile)) {
            $im = imagecreatefromjpeg($uploadFile);

            $ox = imagesx($im);
            $oy = imagesy($im);
            $nx = 200;
            $ny = floor($oy * (200 / $ox));
            $nm = imagecreatetruecolor($nx, $ny);

            imagecopyresized($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);
            imagejpeg($nm, "img/thumbnail/".$cnt.".jpg");

        }else echo "File is not upload!!";

    }else echo "Error #".$_FILES['image']['error'];

}



?>
<!DOCTYPE html>
<html>
<head>
    <title>Photo gallery</title>
</head>
<body>

<h1>Photo gallery</h1>
<?php for ( $i = 1; $i <= ( countImages('img/') ); $i++ ){ ?>
    <a target="_blank" href="img/<?=$i?>.jpg"> <img src='img/thumbnail/<?=$i?>.jpg'></a>
<?php } ?>

<p><?= $error; ?></p><br>
<form enctype="multipart/form-data" method="post">
    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
    Upload photo: <input accept="image/jpeg" name="image" type="file" /><br>
    <input type="submit" value="Send File" />
</form>

</body>
</html>
