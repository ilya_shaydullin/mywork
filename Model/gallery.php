<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 26.02.2016
 * Time: 20:57
 */

$mysqli = new mysqli("192.168.10.10:3306", "homestead", "secret", "work");
//$mysqli = new mysqli("localhost", "user", "q1w2e3r4t5", "work");
if ($mysqli->connect_errno) {
    echo "Не удалось подключиться к MySQL: " . $mysqli->connect_error;
}
//mysqli_select_db($cnt, "homestead");

function countImages($dir)
{
    $count = 0;
    $elements = scandir($dir);
    foreach ($elements as $value) {
        if (($value != '..') && ($value != '.') && !is_dir($dir . $value)) $count++;
    }
    return $count;
}

function fileUpload($file, $name, $description)
{

    if (($file['error'] == 2) || ($file['size'] > 2000000)) {
        echo "File is more 2Mb!";
    } elseif ($file['type'] != "image/jpeg") echo "File is not jpeg!";

    elseif ($file['error'] == 0) {
        $lnk = md5(uniqid(rand(), true));
        $uploadFile = "img/" . $lnk;

        if (move_uploaded_file($file['tmp_name'], $uploadFile)) {
            $im = imagecreatefromjpeg($uploadFile);

            $ox = imagesx($im);
            $oy = imagesy($im);
            $nx = 200;
            $ny = floor($oy * (200 / $ox));
            $nm = imagecreatetruecolor($nx, $ny);

            imagecopyresized($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);
            imagejpeg($nm, "img/thumbnail/" . $lnk);

            global $mysqli;
            $mysqli->autocommit(false);
            if (!($mysqli->query('INSERT INTO `photo` (`name`, `description`, `link`)
VALUES ("' . $name . '", "' . $description . '", "' . $lnk . '" )')) ||
                !($res = $mysqli->query(' SELECT `id` FROM `photo` WHERE `name` = "' . $name . '"')->fetch_assoc()) || //( ( $row = $res->fetch_assoc() ) != "" ) ||
                !$mysqli->query('INSERT INTO `popularity` (`id_photo`) VALUES (' . $res['id'] . ')')
            ) {
                unlink($uploadFile);
                unlink("img/thumbnail/" . $lnk);
                echo "Не удалось сохранить файл: (" . $mysqli->errno . ") " . $mysqli->error . "<br>" . $res['id'];
                $mysqli->rollback();
                //print_r($res);
            } else {
                $mysqli->commit();
            }
            $mysqli->autocommit(true);

        } else echo "File is not upload!!";

    } else echo "Error #" . $file['error'];
}

function getAllPhotos()
{
    global $mysqli;
    if (!($imgs = $mysqli->query(
        'SELECT * FROM `photo`, `popularity`
WHERE `photo`.`id` = `popularity`.`id_photo` ORDER BY `popularity`.`count` DESC'))
    )
        echo "Не удалось загрузить данные: (" . $mysqli->errno . ") " . $mysqli->error;

    else  return $imgs;

}

function getPhotoOnId($id){
    global $mysqli;
     if ( !( $res = $mysqli->query('SELECT * FROM `photo` WHERE `id` = "'.$id.'"' )->fetch_assoc() ) )
         echo "Не удалось загрузить данные: (" . $mysqli->errno . ") " . $mysqli->error;
     else  return $res;
}

function getAndUpPopularity($id){
    global $mysqli;
    $pop = $mysqli->query('SELECT * FROM `popularity` WHERE `id_photo` = "'.$id.'"')->fetch_assoc();
    if ( !$mysqli->query('UPDATE `popularity` SET count = "'.($pop['count'] + 1).'" WHERE `id_photo` = "'.$id.'"') )
        echo "Не удалось загрузить данные: (" . $mysqli->errno . ") " . $mysqli->error;
    else return $pop['count'];
}

function updatePhotoData($id, $name, $description, $title = "", $alt = ""){
    global $mysqli;
        if ( !$mysqli->query('UPDATE `photo` SET `name` = "'.$name.'", `description` = "'.$description.'",
        `title` = "'.$title.'", `alt` = "'.$alt.'"
        WHERE `id` = "'.$id.'"') )
            echo "Не удалось сохранить данные: (" . $mysqli->errno . ") " . $mysqli->error;
        else {echo "Saved!"; header("location: /dz8_photo.php?id=".$id); }
}

function createTables(){
    global $mysqli;
    if (!$mysqli->query("DROP TABLE IF EXISTS `popularity`") ||
        !$mysqli->query("DROP TABLE IF EXISTS `photo`") ||
        !$mysqli->query("CREATE TABLE `photo`(
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(32) NOT NULL,
  `link` varchar(32) NOT NULL,
  `title` varchar(32) NULL,
  `alt` varchar(32) NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;") ||
        !$mysqli->query("CREATE TABLE `popularity`(
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_photo` int(11) UNSIGNED NOT NULL,
  `count` int(11),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_photo`) REFERENCES `photo` (`id`) ON DELETE CASCADE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;")
    ) {
        echo "Не удалось создать таблицу: (" . $mysqli->errno . ") " . $mysqli->error;
    }
}

function insertItems(){
    global $mysqli;
    $mysqli->autocommit(false);
    //for ($i = 1; $i <= (countImages('img/')); $i++)
    $elements = scandir('img/');
    $i = 1;
    foreach ($elements as $value) {
        if (($value != '..') && ($value != '.') && !is_dir('img/' . $value)) {
            if (!($mysqli->query('INSERT INTO `photo` (`name`, `description`, `link`) VALUES ("Photo#' . $i . '", "Description#' . $i . '", "' . $value . '" )')) ||
                !($res = $mysqli->query(' SELECT `id` FROM `photo` WHERE `name` = "Photo#' . $i . '"')->fetch_assoc()) || //( ( $row = $res->fetch_assoc() ) != "" ) ||
                !$mysqli->query('INSERT INTO `popularity` (`id_photo`) VALUES (' . $res['id'] . ')')
            ) {
                echo "Не удалось создать запись: (" . $mysqli->errno . ") " . $mysqli->error . "<br>" . $res['id'];
                $mysqli->rollback();
                //print_r($res);
            } else {
                $mysqli->commit();
            }
            $i++;
        }
    }
    $mysqli->autocommit(true);
}