<?php
/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 26.02.2016
 * Time: 20:53
 */
include "Model/gallery.php";

if ( !isset( $_GET['id'] ) ) header('Location: /dz8.php');

if ( isset( $_POST['name'] ) && isset( $_POST['description'] ) ) {
    updatePhotoData($_GET['id'], $_POST['name'], $_POST['description'], $_POST['title'], $_POST['alt']);
}
$photo = getPhotoOnId( $_GET['id'] );
$pop = getAndUpPopularity($_GET['id']);
$title = $photo['name'];

$content = "View/dz8_photo.php";
include ("View/main.php");